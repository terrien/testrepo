﻿DROP FUNCTION _bp02_pgplsql(character varying);

CREATE OR REPLACE FUNCTION _bp02_pgplsql(IN ps_nom character varying)
  RETURNS TABLE(nom character varying, prenom character varying) AS
$BODY$
--------------------------------------------------------------------------------------------
--- TEST POUR BONNES PRATIQUES
-- 
--             HISTORIQUE DES MODIFICATIONS (Version / qui / date / quoi)
--
-- v5.0.82.0    -    NTE  -  31/10/2014 - Création suite ITV xxxxx
-- v5.0.83.0    -    NTE  -  07/11/2014 - Modification : returns TABLE plutôt que SETOF ... 
--
-- Rmq : cette procédure ne nécessite aucune remarque 
--------------------------------------------------------------------------------------------
BEGIN

RETURN QUERY SELECT UPPER(CLI_NOM)::VARCHAR(50), UPPER(CLI_PRENOM)::VARCHAR(50) 
        FROM CLIENT 
        WHERE lower(CLI_NOM) LIKE lower(ps_nom)
        ORDER BY 1,2;
END;
$BODY$
  LANGUAGE plpgsql STABLE COST 100 ROWS 1000;
/* --- DROITS
ALTER FUNCTION _bp02_pgplsql(character varying) OWNER TO ____;
*/