﻿DROP FUNCTION IF EXISTS _bp01_sql(character varying);

CREATE OR REPLACE FUNCTION _bp01_sql(IN character varying DEFAULT ''::character varying)
  RETURNS TABLE(nom character varying, prenom character varying) AS
$BODY$
--------------------------------------------------------------------------------------------
--- TEST POUR BONNES PRATIQUES
--
--             HISTORIQUE DES MODIFICATIONS (Version / qui / date / quoi)
--
-- v5.0.82.0    -    NTE  -  31/10/2014 - Création suite ITV xxxxx
-- v5.0.83.0    -    NTE  -  12/11/2014 - Modification : ajout du commentaire ...
--
-- Rmq : cette procédure ne nécessite aucune remarque
--------------------------------------------------------------------------------------------
 SELECT UPPER(cli_nom)::VARCHAR(50), UPPER(cli_prenom)::VARCHAR(50)
        FROM CLIENT
        WHERE lower(CLI_NOM) LIKE lower($1)
        ORDER BY 1,2;
$BODY$
  LANGUAGE sql STABLE COST 100 ROWS 1000;
/* --- DROITS
ALTER FUNCTION _bp01_sql(character varying) OWNER TO ____;
*/